#!/usr/bin/env/ python
import xml.etree.ElementTree as ET
import codecs
tree = ET.parse("data/cat_out.txt")
root = tree.getroot()

outfile = codecs.open("data/namematch_fil.txt",encoding="utf-8",mode="w")
count = 0
for page in root:
	count += 1
	outfile.write("%d|%s\n"%(count,page.find("title").text))


