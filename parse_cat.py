#!/usr/bin/env python

import re
import sys
import xml.etree.ElementTree as ET
import codecs

tree = ET.parse('data/data.xml')
root = tree.getroot()

idfile = open("data/overlap_id.txt")
idset = set()
for line in idfile:
	temp = line.split("|")
	idset.add(int(temp[1]))

outfile = codecs.open('data/cat_out.txt', encoding='utf-8',mode='w+')
rootstr = root.tag
namespace = re.search(r'({.*})', rootstr).group(1)

outfile.write('<category>\n')

for page in root.findall(namespace+'page'):
	title = page.find(namespace+'title').text
	idnum = int(page.find(namespace+'id').text)
	if idnum not in idset:
		continue
	outfile.write(u"""<page>
	<title>%s</title>
	<id>%d</id>
	<cat>
""" % (title, idnum))
	text = page.find('.//'+namespace+'text').text
	category_list  = re.findall(u'\[\[Category:(.*)\]\]', text)
	for cat in category_list:
		cat = cat.replace('&nbsp;',' ')
		cat = cat.replace('&','and')
		if re.search('\<\!\-\-', cat):
			continue
		cat2 = cat.encode('utf-8').strip('|* ')
		outfile.write(u'\t\t'+cat2.decode('utf8')+'\n')
	outfile.write(u'\t</cat>\n')
	outfile.write(u'</page>\n')
outfile.write(u'</category>\n')
outfile.close()
