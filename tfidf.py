#!/usr/bin/env python
import sys
import re
import xml.etree.ElementTree as ET
import string
import codecs
import math

#initialization
NUMPAGE = 0
title_list = []
idf = {}
print 'Done initialization'
term_num_docs = {}

#parse text to token
def get_tokens(str):
	return re.findall(r"<a.*?/a>|<[^\>]*>|[\w'@#]+", str)

#calculate idf of 'term'
def get_idf(term):
	global NUMPAGE
	global term_num_docs
	if not term in term_num_docs:
		return 0
	return math.log(float(1+NUMPAGE) / (1+term_num_docs[term]))

def main():
	global NUMPAGE
	#parse the xml file
	tree = ET.parse('data/data_filtered.xml')
	root = tree.getroot()

	#readin the word embedding word list and store in word_list
	wordlist_file = open('data/words.lst')
	word_list = set(wordlist_file.read().split())
	wordlist_file.close()

	#set output file
	outputfile = codecs.open('tfidf_out.txt', encoding='utf-8', mode='w+')

	#count how many times terms shows up and number of pages in xml file
	for page in root.findall('page'):
		text = page.find('text').text
		NUMPAGE += 1
		words = set(get_tokens(text))
		for word in words:
			if word in term_num_docs:
				term_num_docs[word] += 1
			else:
				term_num_docs[word] = 1
	print 'IDF ends!'

	#count TF and calculate tf-idf
	#Use normalized frequency
	outputfile.write('<tfidf>\n')
	counter = 0
	for page in root.findall('page'):
		tfidf = {}
		tf = {}
		counter += 1
		#separate tokens from page text
		tokens = get_tokens(page.find('text').text)
		tokens_set = set(tokens)
		title = page.find('title').text
		idnum = page.find('id').text
		title_list.append(title)
		#output the beginning tags to output file
		outputfile.write('\t<page>\n')
		outputfile.write('\t<title>'+title+'</title>\n')
		outputfile.write('\t<id>'+idnum+'</id>\n')
		print 'This page is %d, title is %s' % (counter, title)

		for word in tokens_set:
			# TF is counted as (# of term occurance / # total tokens)
			thistf = float(tokens.count(word)) / len(tokens)
			thisidf = get_idf(word)
			# Eliminate the words which not appear in word_list
			if word in word_list:
				tfidf[word] = thistf * thisidf
				tf[word] = thistf

		# Output as decreasing order of tfidf value
		for w in sorted(tfidf, key = tfidf.get, reverse=True):
			outputfile.write('\t'+str(w)+'\t'+str(tf[w])+'\t'+str(tfidf[w])+'\n')
		outputfile.write('\t</page>\n')
	outputfile.write('</tfidf>\n')
	outputfile.close()

if __name__ == '__main__':
	main()
