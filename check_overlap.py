#!/usr/bin/env/ python
import re
import xml.etree.ElementTree as ET

id_our = set()
id_their = []

def getdata():
	tree = ET.parse("data/output_data.xml")
	root = tree.getroot()
	for page in root:
		id_our.add(int(page.find("id").text))

	theirfile = open("data/person_sorted.txt")
	count = 0
	linenum = 0
	out = open("data/overlap_id.txt","w")
	for line in theirfile:
		temp = line.split('|')
		temp[1] = int(temp[1])
		if temp[1] in id_our:
			count += 1
			out.write("%d|%s\n" % (temp[1],temp[2]))
		linenum += 1	
		if linenum==25000: break


if __name__=="__main__":
	getdata()

