#!/usr/bin/env/ python
import re
import sys
import xml.etree.ElementTree as ET

tree = ET.parse('data/cat_out.txt')
root = tree.getroot()

id2name = {}
name2id = {}
matchfile = open('data/overlap_id.txt')
for line in matchfile:
	st = line.strip().split('|')
	id2name[int(st[0])] = st[2].decode('utf8')
	name2id[st[2].decode('utf8')] = int(st[0])
matchfile.close()

id2cat = {} 
for page in root.findall('page'):
	text = page.find('cat').text
	cats = re.split(r'[ |\t|\n]*',text)
	cats = filter(lambda a: a!='', cats)
	title = page.find('title').text
	for cat in cats:
		cat = cat.strip()
	id2cat[name2id[title]] = set(cats)
	
totalnum = len(id2name)
print totalnum	
value_mat = [[len(id2cat[i]&id2cat[j]) for j in range(1,totalnum+1)] for i in range(1,totalnum+1)]

print len(value_mat)
print len(value_mat[1])

outfile = open('data/pairvalue.txt','w')

for i in range(1,totalnum+1):
	for j in range(1,totalnum+1):
		outfile.write(str(value_mat[i-1][j-1])+' ')
	outfile.write('\n')

outfile.close()
