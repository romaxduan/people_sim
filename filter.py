#!/usr/bin/env/ python
import xml.etree.ElementTree as ET

tree = ET.parse("data/output_data.xml")
root = tree.getroot()
count = 0
for page in root:
	count += 1
print count

idfile = open("data/overlap_id.txt")
idset = set()
count = 0
for line in idfile:
	count += 1
	st = line.split("|")
	idset.add(st[1])
print count
count = 0

pages = root.findall("page")
for page in pages:
	if page.find("id").text not in idset:
		root.remove(page)
		count += 1
#	count -= 1
#print page.find("title").text
print count
tree.write("data/data_filtered.xml")
